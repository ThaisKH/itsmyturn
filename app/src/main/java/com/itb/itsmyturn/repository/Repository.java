package com.itb.itsmyturn.repository;

import com.itb.itsmyturn.model.AuthToken;
import com.itb.itsmyturn.model.RegisterAuth;
import com.itb.itsmyturn.model.SubjectsList;
import com.itb.itsmyturn.model.TurnList;
import com.itb.itsmyturn.model.TurnResponse;
import com.itb.itsmyturn.service.Service;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Repository {
    private static final String URL = "https://itsmyturnatitb.herokuapp.com/api/v1/";

    private Service service;

    public Repository(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(Service.class);
    }

    public LiveData<TurnList> getTurnsByClasse(int id){
        MutableLiveData<TurnList> turns = new MutableLiveData<>();
        Call<TurnList> call = service.getTurnsByClass(id);
        call.enqueue(new Callback<TurnList>() {
            @Override
            public void onResponse(Call<TurnList> call, Response<TurnList> response) {
                turns.postValue(response.body());
            }

            @Override
            public void onFailure(Call<TurnList> call, Throwable t) {

            }
        });
        return turns;
    }

    public LiveData<SubjectsList> getClasseByTeacher(String email){
        MutableLiveData<SubjectsList> classes = new MutableLiveData<>();
        Call<SubjectsList> call = service.getClassesByTeacher(email);
        call.enqueue(new Callback<SubjectsList>() {
            @Override
            public void onResponse(Call<SubjectsList> call, Response<SubjectsList> response) {
                classes.postValue(response.body());
            }

            @Override
            public void onFailure(Call<SubjectsList> call, Throwable t) {

            }
        });
        return classes;
    }

    public LiveData<SubjectsList> getClasseByStudent(String email){
        MutableLiveData<SubjectsList> classes = new MutableLiveData<>();
        Call<SubjectsList> call = service.getClassesByStudent(email);
        call.enqueue(new Callback<SubjectsList>() {
            @Override
            public void onResponse(Call<SubjectsList> call, Response<SubjectsList> response) {
                classes.postValue(response.body());
            }

            @Override
            public void onFailure(Call<SubjectsList> call, Throwable t) {

            }
        });
        return classes;
    }

    public LiveData<AuthToken> login(String email, String password){
        MutableLiveData<AuthToken> log = new MutableLiveData<>();
        Call<AuthToken> call = service.login(email,password);
        call.enqueue(new Callback<AuthToken>() {
            @Override
            public void onResponse(Call<AuthToken> call, Response<AuthToken> response) {
                log.postValue(response.body());
            }

            @Override
            public void onFailure(Call<AuthToken> call, Throwable t) {

            }
        });
        return log;
    }

    public LiveData<RegisterAuth> register(String name, String email, String password, boolean isTeacher){
        MutableLiveData<RegisterAuth> reg = new MutableLiveData<>();
        Call<RegisterAuth> call = service.register(name,email,password,isTeacher);
        call.enqueue(new Callback<RegisterAuth>() {
            @Override
            public void onResponse(Call<RegisterAuth> call, Response<RegisterAuth> response) {
                reg.postValue(response.body());
            }

            @Override
            public void onFailure(Call<RegisterAuth> call, Throwable t) {

            }
        });
        return reg;
    }

    public LiveData<Boolean> isTeacher(String email){
        MutableLiveData<Boolean> isTeacher = new MutableLiveData<>();
        Call<Boolean> call = service.isTeacher(email);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                isTeacher.postValue(response.body());
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

            }
        });
        return isTeacher;
    }

    public LiveData<String> getNameByEmail(String email){
        MutableLiveData<String> name = new MutableLiveData<>();
        Call<String> call = service.getNameByEmail(email);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                name.postValue(response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
        return name;
    }

    public LiveData<String> insertTurn(String email, int idSubject, String time, String description){
        MutableLiveData<String> inserted = new MutableLiveData<>();
        Call<String> call = service.insertTurn(email,idSubject,time,description);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                inserted.postValue(response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
        return inserted;

    }

    public void addClass(String text, String email) {
        Call<String> call = service.joinClass(text, email);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
    Integer id;
    public Integer createClass(String name, String group, String email){
        Call<Integer> call = service.addClass(name, group, email);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                id = response.body();
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {

            }
        });
        return id;
    }

    public LiveData<String> deleteTurn(int idTurn){
        MutableLiveData<String> deleted = new MutableLiveData<>();
        Call<String> call = service.deleteTurn(idTurn);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                deleted.postValue(response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
        return deleted;
    }

    public LiveData<TurnResponse> getTurnById(int idTurn){
        MutableLiveData<TurnResponse> turn = new MutableLiveData<>();
        Call<TurnResponse> call = service.getTurnById(idTurn);
        call.enqueue(new Callback<TurnResponse>() {
            @Override
            public void onResponse(Call<TurnResponse> call, Response<TurnResponse> response) {
                turn.postValue(response.body());
            }

            @Override
            public void onFailure(Call<TurnResponse> call, Throwable t) {

            }
        });
        return turn;
    }
}
