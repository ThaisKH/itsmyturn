package com.itb.itsmyturn.ui.classes;

import androidx.lifecycle.ViewModel;

import com.itb.itsmyturn.repository.Repository;

public class AddClassViewModel extends ViewModel {
    Repository repository = new Repository();
    public void addClass(String text, String email) {
        repository.addClass(text, email);
    }
}
