package com.itb.itsmyturn.ui.register;

import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.itb.itsmyturn.R;
import com.itb.itsmyturn.model.RegisterAuth;
import com.itb.itsmyturn.model.RegisterUser;


public class RegisterFragment extends Fragment {

    private TextInputLayout tilName;
    private TextInputLayout tilREmail;
    private TextInputLayout tilRPassword;
    private TextInputLayout tilPasswordRepeat;
    private Switch swRole;
    private MaterialButton btnRRegister;
    private MaterialButton btnRLogin;

    private RegisterViewModel mViewModel;

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }
    View view;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tilName = view.findViewById(R.id.tilName);
        tilREmail = view.findViewById(R.id.tilREmail);
        tilRPassword = view.findViewById(R.id.tilRPassword);
        tilPasswordRepeat = view.findViewById(R.id.tilPasswordRepeat);
        swRole = view.findViewById(R.id.swRole);
        btnRRegister = view.findViewById(R.id.btnRRegister);
        btnRLogin = view.findViewById(R.id.btnRLogin);
        this.view = view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);

        btnRRegister.setOnClickListener(this::onViewClicked);
        btnRLogin.setOnClickListener(this::onViewClicked);
    }

    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnRRegister:
                validateRegister(view);
                break;
            case R.id.btnRLogin:
                Navigation.findNavController(view).navigate(R.id.action_registerFragment_to_loginFragment);
                break;
        }
    }

    private void validateRegister(View view) {
        boolean validate = true;
        validate = validateNotEmpty(tilName) && validate;
        validate = validateNotEmpty(tilREmail) && validateEmailWrongFormat(tilREmail) && validate;
        validate = validateNotEmpty(tilRPassword) && validateNotEmpty(tilPasswordRepeat) && validatePasswordsDontMatch(tilRPassword,tilPasswordRepeat) && validate;
        if (validate) RegisterUser(new RegisterUser(tilName.getEditText().getText().toString(), tilREmail.getEditText().getText().toString(), tilRPassword.getEditText().getText().toString(), swRole.isChecked()));
    }

    private void RegisterUser(RegisterUser registerUser) {
        LiveData<RegisterAuth> registerAuthLiveData = mViewModel.register(registerUser);
        registerAuthLiveData.observe(this, this::onRegisterConfirm);
    }

    private void onRegisterConfirm(RegisterAuth registerAuth) {
        Toast.makeText(getContext(), getString(R.string.user_registered),Toast.LENGTH_LONG).show();
        Navigation.findNavController(this.view).navigate(R.id.action_registerFragment_to_loginFragment);
    }

    private boolean validateNotEmpty(TextInputLayout til) {
        til.setError("");
        if (til.getEditText().getText().toString().isEmpty()) {
            til.setError(getString(R.string.required_field));
            scrollTo(til);
            return false;
        }
        return true;
    }

    private boolean validatePasswordsDontMatch(TextInputLayout t1, TextInputLayout t2){
        if(!t1.getEditText().getText().toString().isEmpty() | !t2.getEditText().getText().toString().isEmpty()) {
            t1.setError("");
            t2.setError("");
        }
        if (!t1.getEditText().getText().toString().equals(t2.getEditText().getText().toString())) {
            t1.setError(getString(R.string.passwords_dont_match));
            t2.setError(getString(R.string.passwords_dont_match));
            Toast.makeText(getContext(),getString(R.string.passwords_dont_match), Toast.LENGTH_LONG).show();
            scrollTo(t1);
            scrollTo(t2);
            return false;
        }
        return true;
    }

    private boolean validateEmailWrongFormat(TextInputLayout til){
        til.setError("");
        if(!Patterns.EMAIL_ADDRESS.matcher(til.getEditText().getText().toString()).matches()){
            til.setError(getString(R.string.wrong_email_format));
            scrollTo(til);
            return false;
        }
        return true;
    }

    private void scrollTo(TextInputLayout targetView) {
        targetView.getParent().requestChildFocus(targetView, targetView);
    }
}
