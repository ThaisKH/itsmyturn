package com.itb.itsmyturn.ui.turn;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.itb.itsmyturn.R;
import com.itb.itsmyturn.model.TurnList;
import com.itb.itsmyturn.model.TurnResponse;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TurnAdapter extends RecyclerView.Adapter<TurnAdapter.TurnViewHolder> {
    private TurnList turns;
    private OnTurnClickListener onTurnClickListener;
    private boolean isTeacher;
    private TurnViewModel mViewModel;

    public TurnAdapter() {
    }

    public void setTurns(TurnList turns) {
        this.turns = turns;
        notifyDataSetChanged();
    }

    public void setIsTeacher(boolean teacher) {
        isTeacher = teacher;
    }

    public void setmViewModel(TurnViewModel mViewModel) {
        this.mViewModel = mViewModel;
    }

    public void setOnTurnClickListener(OnTurnClickListener onTurnClickListener) {
        this.onTurnClickListener = onTurnClickListener;
    }

    @NonNull
    @Override
    public TurnViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.turn_item, parent, false);
        return new TurnViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TurnViewHolder holder, int position) {
        TurnResponse turn = turns.getTurnsList().get(position);
        holder.tvStudent.setText(turn.getStudent());
        holder.tvTime.setText(turn.getTime());
        holder.cbAttended.setEnabled(isTeacher);
        holder.cbAttended.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    mViewModel.deleteTurn(turn.getId());
                    holder.tvStudent.setPaintFlags(holder.tvStudent.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.tvTime.setPaintFlags(holder.tvTime.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.itemView.setOnClickListener(null);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(turns != null) {
            if (turns.getTurnsList() != null) size = turns.getTurnsList().size();
        }
        return size;
    }

    public class TurnViewHolder extends RecyclerView.ViewHolder{
        TextView tvStudent;
        TextView tvTime;
        CheckBox cbAttended;

        public TurnViewHolder(@NonNull View view) {
            super(view);

            tvStudent = view.findViewById(R.id.tvStudent);
            tvTime = view.findViewById(R.id.tvTime);
            cbAttended = view.findViewById(R.id.cbAttended);
            view.setOnClickListener(this::onTurnClicked);
        }
        public void onTurnClicked(View view){
            TurnResponse turn = turns.getTurnsList().get(getAdapterPosition());
            onTurnClickListener.onTurnClicked(turn);
        }
    }

    public interface OnTurnClickListener{
        void onTurnClicked(TurnResponse turn);
    }
}
