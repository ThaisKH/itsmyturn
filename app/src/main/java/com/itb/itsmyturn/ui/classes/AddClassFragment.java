package com.itb.itsmyturn.ui.classes;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.textfield.TextInputLayout;
import com.itb.itsmyturn.R;

public class AddClassFragment extends Fragment {

    private AddClassViewModel mViewModel;

    public static AddClassFragment newInstance() {
        return new AddClassFragment();
    }
    TextInputLayout tilIdClase;
    Button addButton;
    String email;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_class_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tilIdClase = view.findViewById(R.id.tilAddClass);
        addButton = view.findViewById(R.id.buttonAddClass);
        email = AddClassFragmentArgs.fromBundle(getArguments()).getEmail();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AddClassViewModel.class);
        addButton.setOnClickListener(this::onButtonClicked);
    }

    private void onButtonClicked(View view) {
        String text = tilIdClase.getEditText().getText().toString();
        if (text.isEmpty()){
            tilIdClase.setError(getString(R.string.required_field));
        }else{
            mViewModel.addClass(text, email);
            NavDirections action = AddClassFragmentDirections.actionAddClassFragmentToClassesFragment(email);
            Navigation.findNavController(getView()).navigate(action);
        }
    }

}
