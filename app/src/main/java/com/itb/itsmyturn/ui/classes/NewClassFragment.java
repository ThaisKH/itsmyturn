package com.itb.itsmyturn.ui.classes;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.textfield.TextInputLayout;
import com.itb.itsmyturn.R;

public class NewClassFragment extends Fragment {

    private NewClassViewModel mViewModel;

    public static NewClassFragment newInstance() {
        return new NewClassFragment();
    }
    private TextInputLayout subject, group;
    private String email;
    private Button button;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_class_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        subject= view.findViewById(R.id.tilNewClass);
        group = view.findViewById(R.id.tilGroupName);
        email = NewClassFragmentArgs.fromBundle(getArguments()).getEmail();
        button = view.findViewById(R.id.newButton);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(NewClassViewModel.class);

        button.setOnClickListener(this::onButtonClicked);
    }

    private void onButtonClicked(View view) {
        boolean validate = true;
        subject.setError("");
        group.setError("");
        validate = validateNotEmpty(subject, validate);
        validate = validateNotEmpty(group, validate);
        if (validate){
            mViewModel.newSubject(subject.getEditText().getText().toString(), group.getEditText().getText().toString(), email);
            NavDirections action = NewClassFragmentDirections.actionNewClassFragmentToClassesFragment(email);
            Navigation.findNavController(getView()).navigate(action);
        }
    }

    private boolean validateNotEmpty(TextInputLayout layout, boolean validate) {
        if (layout.getEditText().getText().toString().isEmpty()) {
            layout.setError(getString(R.string.required_field));
            scrollTo(layout);
            validate = false;
        }
        return validate;
    }

    private void scrollTo(TextInputLayout targetView) {
        targetView.getParent().requestChildFocus(targetView, targetView);
    }

}
