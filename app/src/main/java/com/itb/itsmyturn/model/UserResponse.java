package com.itb.itsmyturn.model;

public class UserResponse {
    private boolean isTeacher;
    private String authToken;

    public UserResponse(boolean isTeacher, String authToken) {
        this.isTeacher = isTeacher;
        this.authToken = authToken;
    }

    public boolean isTeacher() {
        return isTeacher;
    }

    public void setTeacher(boolean teacher) {
        isTeacher = teacher;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
}
