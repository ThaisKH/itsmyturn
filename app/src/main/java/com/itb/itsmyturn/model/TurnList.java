package com.itb.itsmyturn.model;

import java.io.Serializable;
import java.util.List;

public class TurnList implements Serializable {
    private List<TurnResponse> turnsList;

    public TurnList(List<TurnResponse> turnsList) {
        this.turnsList = turnsList;
    }

    public List<TurnResponse> getTurnsList() {
        return turnsList;
    }

    public void setTurnsList(List<TurnResponse> turnsList) {
        this.turnsList = turnsList;
    }
}
