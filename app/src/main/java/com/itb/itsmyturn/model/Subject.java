package com.itb.itsmyturn.model;

public class Subject {
    private int id;
    private int idTeacher;
    private String subjectName;
    private String userGroup;

    public Subject() {
    }

    public Subject(int id, int idTeacher, String subjectName, String userGroup) {
        this.id = id;
        this.idTeacher = idTeacher;
        this.subjectName = subjectName;
        this.userGroup = userGroup;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTeacher() {
        return idTeacher;
    }

    public void setIdTeacher(int idTeacher) {
        this.idTeacher = idTeacher;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }
}
