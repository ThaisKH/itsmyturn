# Títol del projecte
## It's My Turn!

# Membres
- Thais Camuñez
- Esteban García
- Rubén Gómez

# Descripció
Aplicació per demanar torn amb el professor a classe. S'utilitzará quan un professor deixi temps als alumnes per fer tasques i aquest vulguin demanar-li ajuda, els alumnes demanaran l'atenció del professor a través de l'aplicació i aquest haurá de seguir l'ordre de torns demanats per poder atendre a tothom en l'ordre que han demanat l'ajuda.

# Públic objectiu 
- Estudiants
- Professors

# Motivació i finalitat

# Link al Figma
https://www.figma.com/file/UcszpGmdOVO9Ug5gR5lr4b/Material-Baseline-Design-Kit?node-id=166%3A6744